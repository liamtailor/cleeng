-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Czas generowania: 10 Cze 2020, 18:13
-- Wersja serwera: 10.3.14-MariaDB
-- Wersja PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `cleeng`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `entitlement`
--

DROP TABLE IF EXISTS `entitlement`;
CREATE TABLE IF NOT EXISTS `entitlement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `expiration_date` date NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `entitlement_set_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FA44802171F7E88B` (`event_id`),
  KEY `IDX_FA448021DA1AC421` (`entitlement_set_id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `entitlement`
--

INSERT INTO `entitlement` (`id`, `expiration_date`, `event_id`, `entitlement_set_id`) VALUES
(1, '2020-06-13', 2, NULL),
(2, '2020-06-14', 1, NULL),
(3, '2020-06-13', 2, NULL),
(4, '2020-06-11', 3, NULL),
(5, '2020-06-12', 4, NULL),
(6, '2020-06-14', 1, NULL),
(7, '2020-06-11', 3, NULL),
(8, '2020-06-15', 1, NULL),
(9, '2020-06-12', 3, NULL),
(10, '2020-06-14', 2, NULL),
(11, '2020-06-12', 3, NULL),
(12, '2020-06-14', 4, NULL),
(13, '2020-06-16', 1, NULL),
(14, '2020-06-14', 4, NULL),
(15, '2020-06-16', 1, NULL),
(16, '2020-06-14', 4, NULL),
(17, '2020-06-16', 1, NULL),
(18, '2020-06-14', 4, NULL),
(19, '2020-06-16', 1, NULL),
(20, '2020-06-14', 4, NULL),
(21, '2020-06-16', 1, NULL),
(22, '2020-06-14', 4, NULL),
(23, '2020-06-16', 1, NULL),
(24, '2020-06-14', 4, NULL),
(25, '2020-06-16', 1, NULL),
(26, '2020-06-14', 4, NULL),
(27, '2020-06-16', 1, NULL),
(28, '2020-06-14', 4, NULL),
(29, '2020-06-16', 1, NULL),
(30, '2020-06-14', 4, NULL),
(31, '2020-06-16', 1, NULL),
(32, '2020-06-14', 4, NULL),
(33, '2020-06-16', 1, NULL),
(34, '2020-06-14', 4, NULL),
(35, '2020-06-16', 1, NULL),
(36, '2020-06-14', 4, NULL),
(37, '2020-06-16', 1, NULL),
(38, '2020-06-14', 4, NULL),
(39, '2020-06-16', 1, NULL),
(40, '2020-06-15', 2, NULL),
(41, '2020-06-13', 3, NULL),
(42, '2020-06-16', 1, NULL),
(43, '2020-06-14', 4, NULL),
(44, '2020-06-16', 1, NULL),
(45, '2020-06-14', 4, NULL),
(46, '2020-06-16', 1, NULL),
(47, '2020-06-14', 4, NULL),
(48, '2020-06-16', 1, NULL),
(49, '2020-06-14', 4, NULL),
(50, '2020-06-16', 1, NULL),
(51, '2020-06-14', 4, NULL),
(52, '2020-06-16', 1, NULL),
(53, '2020-06-14', 4, NULL),
(54, '2020-06-16', 1, NULL),
(55, '2020-06-14', 4, NULL),
(56, '2020-06-16', 1, NULL),
(57, '2020-06-15', 2, NULL),
(58, '2020-06-13', 3, NULL),
(59, '2020-06-16', 1, NULL),
(60, '2020-06-14', 4, NULL),
(61, '2020-07-05', 5, NULL),
(62, '2020-06-16', 1, NULL),
(63, '2020-06-15', 2, NULL),
(64, '2020-06-13', 3, NULL),
(65, '2020-06-14', 4, NULL),
(66, '2020-06-13', 3, NULL),
(67, '2020-07-09', 1, NULL),
(68, '2020-07-09', 4, NULL),
(69, '2020-07-09', 2, NULL),
(70, '2020-07-09', 5, NULL),
(71, '2020-07-09', 3, NULL),
(72, '2020-06-15', 4, NULL),
(73, '2020-07-10', 1, NULL),
(74, '2020-07-10', 4, NULL),
(75, '2020-07-10', 2, NULL),
(76, '2020-07-10', 5, NULL),
(77, '2020-07-10', 3, NULL),
(78, '2020-06-16', 2, NULL),
(79, '2020-06-14', 3, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `entitlement_set`
--

DROP TABLE IF EXISTS `entitlement_set`;
CREATE TABLE IF NOT EXISTS `entitlement_set` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `entitlement_set`
--

INSERT INTO `entitlement_set` (`id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `event`
--

DROP TABLE IF EXISTS `event`;
CREATE TABLE IF NOT EXISTS `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` int(11) NOT NULL,
  `access_time` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_3BAE0AA75E237E06` (`name`),
  KEY `IDX_3BAE0AA7B9CF4E62` (`event_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `event`
--

INSERT INTO `event` (`id`, `price`, `access_time`, `name`, `event_category_id`) VALUES
(1, 10, 7, 'Event A', 1),
(2, 3, 6, 'Another Event', 2),
(3, 55, 4, 'Exclusive EVENT', 3),
(4, 123, 5, 'Can\'t Miss', 1),
(5, 10, 26, 'SOME NEW EVENT', 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `event_category`
--

DROP TABLE IF EXISTS `event_category`;
CREATE TABLE IF NOT EXISTS `event_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `event_category`
--

INSERT INTO `event_category` (`id`, `name`) VALUES
(1, 'Sport'),
(2, 'Technology'),
(3, 'Business');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20200606113144', '2020-06-06 11:31:55'),
('20200606120759', '2020-06-06 12:08:19'),
('20200606124338', '2020-06-06 12:43:44'),
('20200606141752', '2020-06-06 14:18:16'),
('20200606143305', '2020-06-07 10:43:18'),
('20200607104322', '2020-06-07 10:43:34'),
('20200607134536', '2020-06-07 13:45:56'),
('20200607183357', '2020-06-07 18:34:24'),
('20200608162031', '2020-06-08 16:21:09'),
('20200608181607', '2020-06-08 18:16:50'),
('20200609154509', '2020-06-09 15:45:56'),
('20200609163304', '2020-06-09 16:34:39'),
('20200609190051', '2020-06-09 19:01:52'),
('20200609192807', '2020-06-09 19:28:47');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `plan`
--

DROP TABLE IF EXISTS `plan`;
CREATE TABLE IF NOT EXISTS `plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_override` int(11) DEFAULT NULL,
  `access_time_override` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_DD5A5B7D5E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `plan`
--

INSERT INTO `plan` (`id`, `name`, `price_override`, `access_time_override`) VALUES
(4, 'SILVER_PLAN', 1000, NULL),
(7, 'TEK AND SPORT', NULL, NULL),
(8, 'SEASON PASS', 1000, 30);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `plan_event_category`
--

DROP TABLE IF EXISTS `plan_event_category`;
CREATE TABLE IF NOT EXISTS `plan_event_category` (
  `plan_id` int(11) NOT NULL,
  `event_category_id` int(11) NOT NULL,
  PRIMARY KEY (`plan_id`,`event_category_id`),
  KEY `IDX_FF7E901BE899029B` (`plan_id`),
  KEY `IDX_FF7E901BB9CF4E62` (`event_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `plan_event_category`
--

INSERT INTO `plan_event_category` (`plan_id`, `event_category_id`) VALUES
(4, 3),
(7, 1),
(7, 2),
(8, 1),
(8, 2),
(8, 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D6495E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`id`, `name`) VALUES
(6, 'Dudeman'),
(5, 'Poorman'),
(3, 'Richman'),
(7, 'Someguy'),
(8, 'testinger'),
(11, 'Testingerator'),
(12, 'Testingeratora'),
(13, 'Testingeratorix'),
(9, 'testingerer'),
(10, 'testingererer');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_entitlement`
--

DROP TABLE IF EXISTS `user_entitlement`;
CREATE TABLE IF NOT EXISTS `user_entitlement` (
  `user_id` int(11) NOT NULL,
  `entitlement_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`entitlement_id`),
  KEY `IDX_535F49B4A76ED395` (`user_id`),
  KEY `IDX_535F49B415FCF4DF` (`entitlement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `user_entitlement`
--

INSERT INTO `user_entitlement` (`user_id`, `entitlement_id`) VALUES
(6, 8),
(13, 79);

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `entitlement`
--
ALTER TABLE `entitlement`
  ADD CONSTRAINT `FK_FA44802171F7E88B` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  ADD CONSTRAINT `FK_FA448021DA1AC421` FOREIGN KEY (`entitlement_set_id`) REFERENCES `entitlement_set` (`id`);

--
-- Ograniczenia dla tabeli `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `FK_3BAE0AA7B9CF4E62` FOREIGN KEY (`event_category_id`) REFERENCES `event_category` (`id`);

--
-- Ograniczenia dla tabeli `plan_event_category`
--
ALTER TABLE `plan_event_category`
  ADD CONSTRAINT `FK_FF7E901BB9CF4E62` FOREIGN KEY (`event_category_id`) REFERENCES `event_category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_FF7E901BE899029B` FOREIGN KEY (`plan_id`) REFERENCES `plan` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `user_entitlement`
--
ALTER TABLE `user_entitlement`
  ADD CONSTRAINT `FK_535F49B415FCF4DF` FOREIGN KEY (`entitlement_id`) REFERENCES `entitlement` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_535F49B4A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
