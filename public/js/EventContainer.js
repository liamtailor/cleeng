class EventContainer {
    eventEntity;
    buyButton;
    unsubscribeButton;
    showButton;
    constructor(eventEntity, buyButton, unsubscribeButton, showButton) {
        this.eventEntity = eventEntity;
        this.buyButton = buyButton;
        this.unsubscribeButton = unsubscribeButton;
        this.showButton = showButton;
    }
}