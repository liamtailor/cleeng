class API {

    /**
     * @param {string} url
     * @param {function} onloadCallback
     */
    get(url, onloadCallback) {
        this._call("GET", url, onloadCallback)
    }

    /**
     * @param {String} url
     * @param {Function} onloadCallback
     * @param {Object} postBody
     */
    post(url, onloadCallback, postBody = null) {
        this._call("POST", url, onloadCallback, postBody)
    }

    /**
     * @param {string} url
     * @param {function} onloadCallback
     */
    put(url, onloadCallback) {
        this._call("PUT", url, onloadCallback)
    }

    /**
     * @param {String} method
     * @param {String} url
     * @param {Function} onloadCallback
     * @param {Object} postBody
     */
    _call(method, url, onloadCallback, postBody = null) {
        const xhr = new XMLHttpRequest();
        url = url.replace(/\/$/, "");
        xhr.open(method, url, true);
        xhr.onload = function () {
            if (xhr.status !== 500) {
                onloadCallback(xhr);
            } else {
                console.log("Request failed.  Status: " + xhr.status);
                return "";
            }
        };
        xhr.send(JSON.stringify(postBody));
        return xhr;
    }
}
