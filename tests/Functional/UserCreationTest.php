<?php

namespace Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserCreationTest extends WebTestCase
{
    public function testPlanCreation()
    {
        $client = static::createClient();
        $postParameters =
            [
                'name' => 'Somebody Oncetoldme'
            ];
        $client->request('POST', '/rest/user', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode($postParameters));

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }
}
