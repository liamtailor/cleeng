<?php

namespace Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PlanCreationTest extends WebTestCase
{
    public function testPlanCreation()
    {
        $client = static::createClient();
        $postParameters =
            [
                'name' => 'SILVER_PLAN',
                'priceOverride' => '1000',
                'accessTimeOverride' => '30',
                'eventCategories' => [

                ]
            ];
        $client->request('POST', '/rest/plan', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode($postParameters));

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }
}
