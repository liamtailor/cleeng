<?php

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LoginPageTest extends WebTestCase
{
    public function testLoginPage_IsAccessible()
    {
        $client = static::createClient();

        $client->request('GET', '/login');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testLoginPage_CanLogin()
    {
        $client = static::createClient();

        $client->request('POST', '/login/13');

        $this->assertEquals(204, $client->getResponse()->getStatusCode());
    }
}
