<?php

use App\Entity\Entitlement;
use App\Entity\Event;
use App\Entity\EventCategory;
use App\Entity\Plan;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class PlanTest extends TestCase
{

    public function testUnSubscribe_RemovesEntitlementForEventFromUser()
    {
        $entitlement = $this->createMock(Entitlement::class);
        $user = $this->createMock(User::class);

        //Expected
        $user->expects(self::once())->method('removeEntitlement')->with($entitlement);

        $user->method('getEntitlements')->willReturn(new ArrayCollection([$entitlement]));
        $event = $this->createMock(Event::class);
        $event->method('getEntitlements')->willReturn(new ArrayCollection([$entitlement]));
        $entitlement->method('getEvent')->willReturn($event);
        $eventCategory = $this->createMock(EventCategory::class);
        $eventCategory->method('getEvents')->willReturn(new ArrayCollection([$event]));

        $plan = new Plan();
        $plan->addEventCategory($eventCategory);
        $plan->unSubscribe($user);
    }
}
