<?php


namespace App\Form;


use App\Entity\EventCategory;
use App\Entity\Plan;
use App\Repository\EventCategoryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('priceOverride', NumberType::class, [
                'required' => false,
            ])
            ->add('accessTimeOverride', NumberType::class, [
                'required' => false,
            ])
            ->add('eventCategories', EntityType::class, [
                'required' => false,
                'class' => EventCategory::class,
                'multiple' => true,
                'query_builder' => function (EventCategoryRepository $repository) {
                    return $repository->createQueryBuilder('e')
                        ->orderBy('e.name', 'ASC');
                },
            ])
            ->add('RESET', ResetType::class)
            ->add('SUBMIT', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Plan::class,
        ]);
    }
}