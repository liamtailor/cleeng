<?php

namespace App\Controller;

use App\Api\EntityFactoryService;
use App\Entity\EventCategory;
use App\Form\EventCategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EventCategoryController extends AbstractController
{
    /**
     * @var EntityFactoryService
     */
    private $entityFactoryService;

    public function __construct(EntityFactoryService $entityFactoryService)
    {
        $this->entityFactoryService = $entityFactoryService;
    }

    /**
     * @Route("/create-event-category", name="create-event-category")
     * @param Request $request
     * @return Response
     * @throws \ReflectionException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\RuntimeException
     * @throws \Symfony\Component\HttpFoundation\Exception\BadRequestException
     */
    public function index(Request $request)
    {
        $form = $this->createForm(EventCategoryType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $newEntity = $this->entityFactoryService->createEntity($form, EventCategory::class);
        }

        return $this->render('event_category/index.html.twig', ['eventCategoryForm' => $form->createView(),]);
    }
}
