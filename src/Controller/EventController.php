<?php

namespace App\Controller;

use App\Entity\Event;
use App\Form\EventType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EventController extends AbstractController
{
    /**
     * @Route("/create-event", name="create-event")
     * @param Request $request
     * @return Response
     * @throws \LogicException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\RuntimeException
     */
    public function index(Request $request)
    {
        $form = $this->createForm(EventType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newEvent = $form->getData();
            if ($newEvent instanceof Event) {
                $entityManger = $this->getDoctrine()->getManager();
                $entityManger->persist($newEvent);
                $entityManger->flush();
            }
        }

        return $this->render('event/index.html.twig', ['eventForm' => $form->createView(),]);
    }
}
