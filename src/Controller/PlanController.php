<?php

namespace App\Controller;

use App\Api\EntityFactoryService;
use App\Entity\Plan;
use App\Form\PlanType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PlanController extends AbstractController
{
    /**
     * @var EntityFactoryService
     */
    private $entityFactoryService;

    public function __construct(EntityFactoryService $entityFactoryService)
    {
        $this->entityFactoryService = $entityFactoryService;
    }

    /**
     * @Route("/create-plan", name="create-plan")
     * @param Request $request
     * @return Response
     * @throws \ReflectionException
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\RuntimeException
     * @throws \Symfony\Component\HttpFoundation\Exception\BadRequestException
     */
    public function index(Request $request)
    {
        $form = $this->createForm(PlanType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $newEntity = $this->entityFactoryService->createEntity($form, Plan::class);
        }

        return $this->render('plan/index.html.twig', ['planForm' => $form->createView(),]);
    }
}
