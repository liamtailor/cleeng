<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{

    /**
     * @Route("/", name="homepage")
     * @return Response
     * @throws \LogicException
     */
    public function index(): Response
    {
        /** @var SessionInterface $session */
        $session = $this->get('session');
        $userId = $session->get('user_id');

        if ($userId) {
            $repository = $this->getDoctrine()->getRepository(User::class);
            $user = $repository->find($userId);

            if ($user) {
                return $this->render('homepage/index.html.twig', ['user' => $user]);
            }
        }

        return $this->redirectToRoute('login-page');
    }
}
