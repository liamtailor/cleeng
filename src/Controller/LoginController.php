<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class LoginController extends AbstractController
{

    /**
     * @Route("/login", name="login-page", methods={"GET"})
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('login/login.html.twig');
    }

    /**
     * @Route("/login/{id}", name="login", methods={"POST"}, requirements={"id"="\d+"})
     * @param int|null $id
     * @return JsonResponse
     */
    public function login(int $id = null): JsonResponse
    {
        try {
            /** @var SessionInterface $session */
            $session = $this->get('session');

            if ($id) {
                $session->set('user_id', $id);
                return $this->json('', 204);
            } else {
                throw new NotFoundHttpException('User not found.');
            }
        } catch (NotFoundHttpException $e) {
            return $this->json(['error' => $e->getMessage()], 404);
        }
    }
}
