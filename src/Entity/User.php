<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity("name")
 */
class User extends ApiEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=Entitlement::class, cascade={"persist"})
     */
    private $entitlements;

    public function __construct()
    {
        $this->entitlements = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Entitlement[]
     */
    public function getEntitlements(): Collection
    {
        return $this->entitlements;
    }

    public function addEntitlement(Entitlement $entitlement): self
    {
        if (!$this->entitlements->contains($entitlement)) {
            $this->entitlements[] = $entitlement;
        }

        return $this;
    }

    public function removeEntitlement(Entitlement $entitlement): self
    {
        if ($this->entitlements->contains($entitlement)) {
            $this->entitlements->removeElement($entitlement);
        }

        return $this;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'entitlements' => $this->getEntitlements()->toArray(),
        ];
    }
}
