<?php


namespace App\Entity;


abstract class ApiEntity implements \JsonSerializable
{
    /**
     * @var int|null
     */
    protected $id;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
}