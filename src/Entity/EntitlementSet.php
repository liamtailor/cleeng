<?php

namespace App\Entity;

use App\Repository\EntitlementSetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EntitlementSetRepository::class)
 */
class EntitlementSet implements \JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=Entitlement::class, mappedBy="entitlementSet", cascade={"persist"})
     */
    private $entitlements;

    /**
     * EntitlementSet constructor.
     * @param Entitlement[] $entitlements
     * @throws \LogicException
     */
    public function __construct(array $entitlements = [])
    {
        foreach ($entitlements as $entitlement) {
            $storedClass = Entitlement::class;
            if (get_class($entitlement) !== $storedClass) {
                throw new \LogicException('This class stores only ' . $storedClass);
            }
        }
        $this->entitlements = new ArrayCollection($entitlements);
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'entitlements' => $this->getEntitlements()->toArray(),
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Entitlement[]
     */
    public function getEntitlements(): Collection
    {
        return $this->entitlements;
    }

    public function addEntitlement(Entitlement $entitlement): self
    {
        if (!$this->entitlements->contains($entitlement)) {
            $this->entitlements[] = $entitlement;
            $entitlement->setEntitlementSet($this);
        }

        return $this;
    }

    public function removeEntitlement(Entitlement $entitlement): self
    {
        if ($this->entitlements->contains($entitlement)) {
            $this->entitlements->removeElement($entitlement);
            // set the owning side to null (unless already changed)
            if ($entitlement->getEntitlementSet() === $this) {
                $entitlement->setEntitlementSet(null);
            }
        }

        return $this;
    }
}
