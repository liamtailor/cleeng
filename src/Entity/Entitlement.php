<?php

namespace App\Entity;

use App\Repository\EntitlementRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EntitlementRepository::class)
 */
class Entitlement extends ApiEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @return int|null
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity=Event::class, cascade="all")
     */
    private $event;

    /**
     * @ORM\Column(type="date")
     */
    private $expirationDate;

    /**
     * @ORM\ManyToOne(targetEntity=EntitlementSet::class, inversedBy="entitlements")
     */
    private $entitlementSet;

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'event' => $this->getEvent(),
            'expirationDate' => $this->getExpirationDate(),
            'isNotExpired' => $this->isNotExpired(),
            'entitlementsSet' => $this->getEntitlementSet()
        ];
    }

    public function getExpirationDate(): ?\DateTimeInterface
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(\DateTimeInterface $expirationDate): self
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function isNotExpired(): bool
    {
        $diffFromCurrentDate = $this->getExpirationDate()->diff(new \DateTime());
        $differenceInDays = $diffFromCurrentDate->d;
        return $differenceInDays >= 0;
    }

    public function getEntitlementSet(): ?EntitlementSet
    {
        return $this->entitlementSet;
    }

    public function setEntitlementSet(?EntitlementSet $entitlementSet): self
    {
        $this->entitlementSet = $entitlementSet;

        return $this;
    }
}
