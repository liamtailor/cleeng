<?php

namespace App\Entity;

use App\Repository\EventRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

/**
 * @ORM\Entity(repositoryClass=EventRepository::class)
 */
class Event extends ApiEntity implements ISubscribable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     */
    private $accessTime;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=Entitlement::class, mappedBy="event", cascade={"persist"})
     */
    private $entitlements;

    /**
     * @ORM\ManyToOne(targetEntity=EventCategory::class, inversedBy="events")
     */
    private $eventCategory;

    public function __toString()
    {
        return $this->getName();
    }

    public function __construct()
    {
        $this->entitlements = new ArrayCollection();
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'price' => $this->getPrice(),
            'accessTime' => $this->getAccessTime()
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getAccessTime(): ?int
    {
        return $this->accessTime;
    }

    public function setAccessTime(int $accessTime): self
    {
        $this->accessTime = $accessTime;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Entitlement[]
     */
    public function getEntitlements(): Collection
    {
        return $this->entitlements;
    }

    public function addEntitlement(Entitlement $entitlement): self
    {
        if (!$this->entitlements->contains($entitlement)) {
            $this->entitlements[] = $entitlement;
            $entitlement->setEvent($this);
        }

        return $this;
    }

    public function removeEntitlement(Entitlement $entitlement): self
    {
        if ($this->entitlements->contains($entitlement)) {
            $this->entitlements->removeElement($entitlement);
            $entitlement->setEvent(null);
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function subscribe(User $user): array
    {
        foreach ($user->getEntitlements() as $entitlement) {
            if ($entitlement->getEvent() === $this) {
                throw new BadRequestException('User is already subscribed to event ' . $this->getName());
            }
        }

        $newEntitlement = new Entitlement();
        $newEntitlement->setEvent($this);
        $newEntitlement->setExpirationDate(new \DateTime("+" . $this->getAccessTime() . " day " . (new \DateTime())->format('Y-m-d')));
        $user->addEntitlement($newEntitlement);

        return [$newEntitlement];
    }

    public function unSubscribe(User $user): void
    {
        foreach ($user->getEntitlements() as $entitlement) {
            if ($entitlement->getEvent() === $this) {
                $user->removeEntitlement($entitlement);
            }
        }
    }

    public function getEventCategory(): ?EventCategory
    {
        return $this->eventCategory;
    }

    public function setEventCategory(?EventCategory $eventCategory): self
    {
        $this->eventCategory = $eventCategory;

        return $this;
    }
}
