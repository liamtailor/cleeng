<?php


namespace App\Entity;


use Symfony\Component\HttpFoundation\Exception\BadRequestException;

interface ISubscribable
{
    public function getId(): ?int;

    /**
     * @param User $user
     * @return Entitlement[]
     * @throws BadRequestException
     * @throws \Exception
     */
    public function subscribe(User $user): array;

    public function unSubscribe(User $user): void;

    public function getPrice(): ?int;
}