<?php

namespace App\Entity;

use App\Repository\PlanRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PlanRepository::class)
 */
class Plan extends ApiEntity implements ISubscribable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $priceOverride;

    /**
     * @ORM\ManyToMany(targetEntity=EventCategory::class, inversedBy="plans")
     */
    private $eventCategories;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $accessTimeOverride;

    public function __construct()
    {
        $this->eventCategories = new ArrayCollection();
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'price' => $this->getPrice(),
            'accessTimeOverride' => $this->getAccessTimeOverride(),
            'eventCategories' => $this->getEventCategories()->toArray(),
        ];
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPriceOverride(): ?int
    {
        return $this->priceOverride;
    }

    public function setPriceOverride(?int $priceOverride): self
    {
        $this->priceOverride = $priceOverride;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function subscribe(User $user): array
    {
        /** @var EventCategory $eventCategory */
        foreach ($this->eventCategories as $eventCategory) {
            foreach ($eventCategory->getEvents() as $event) {
                $newEntitlements = $event->subscribe($user);
                foreach ($newEntitlements as $entitlement) {
                    $planSubscriptionEntitlements[] = $entitlement;
                }
            }
        }
        return $planSubscriptionEntitlements ?? [];
    }

    public function unSubscribe(User $user): void
    {
        foreach ($user->getEntitlements() as $entitlement) {
            $userEntitledEvent = $entitlement->getEvent();
            $thisPlanEvents = $this->getAllEvents();
            if (in_array($userEntitledEvent, $thisPlanEvents)) {
                $user->removeEntitlement($entitlement);
            }
        }
    }

    public function getPrice(): int
    {
        $priceOverride = $this->getPriceOverride();
        if (is_null($priceOverride)) {
            $eventsPricesSum = 0;
            if (!empty($this->eventCategories)) {
                foreach ($this->getAllEvents() as $event) {
                    $eventsPricesSum += $event->getPrice();
                }
            }

            return $eventsPricesSum;
        } else {
            return $priceOverride;
        }

    }

    /**
     * @return Collection|EventCategory[]
     */
    public function getEventCategories(): Collection
    {
        return $this->eventCategories;
    }


    public function addEventCategory(EventCategory $eventCategory): self
    {
        if (!$this->eventCategories->contains($eventCategory)) {
            $this->eventCategories[] = $eventCategory;
        }

        return $this;
    }


    public function removeEventCategory(EventCategory $eventCategory): self
    {
        if ($this->eventCategories->contains($eventCategory)) {
            $this->eventCategories->removeElement($eventCategory);
        }

        return $this;
    }

    /**
     * @return Event[]
     */
    protected function getAllEvents(): array
    {
        $events = [];
        /** @var EventCategory $eventCategory */
        foreach ($this->getEventCategories() as $eventCategory) {
            $eventCategoryEvents = $eventCategory->getEvents()->toArray();
            $events = array_merge($events, $eventCategoryEvents);
        }
        return $events;
    }

    public function getAccessTimeOverride(): ?int
    {
        return $this->accessTimeOverride;
    }

    public function setAccessTimeOverride(?int $accessTimeOverride): self
    {
        $this->accessTimeOverride = $accessTimeOverride;

        return $this;
    }
}
