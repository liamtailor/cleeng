<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200609163304 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE event_category_event (event_category_id INT NOT NULL, event_id INT NOT NULL, INDEX IDX_CD9F39DB9CF4E62 (event_category_id), INDEX IDX_CD9F39D71F7E88B (event_id), PRIMARY KEY(event_category_id, event_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE event_category_event ADD CONSTRAINT FK_CD9F39DB9CF4E62 FOREIGN KEY (event_category_id) REFERENCES event_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE event_category_event ADD CONSTRAINT FK_CD9F39D71F7E88B FOREIGN KEY (event_id) REFERENCES event (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE event_event_category');
        $this->addSql('ALTER TABLE entitlement CHANGE event_id event_id INT DEFAULT NULL, CHANGE entitlement_set_id entitlement_set_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE plan CHANGE price_override price_override INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE event_event_category (event_id INT NOT NULL, event_category_id INT NOT NULL, INDEX IDX_9FE44662B9CF4E62 (event_category_id), INDEX IDX_9FE4466271F7E88B (event_id), PRIMARY KEY(event_id, event_category_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE event_event_category ADD CONSTRAINT FK_9FE4466271F7E88B FOREIGN KEY (event_id) REFERENCES event (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE event_event_category ADD CONSTRAINT FK_9FE44662B9CF4E62 FOREIGN KEY (event_category_id) REFERENCES event_category (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE event_category_event');
        $this->addSql('ALTER TABLE entitlement CHANGE event_id event_id INT DEFAULT NULL, CHANGE entitlement_set_id entitlement_set_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE plan CHANGE price_override price_override INT DEFAULT NULL');
    }
}
