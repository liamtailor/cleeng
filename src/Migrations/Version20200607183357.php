<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200607183357 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entitlement CHANGE event_id event_id INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3BAE0AA75E237E06 ON event (name)');
        $this->addSql('ALTER TABLE plan CHANGE price_override price_override INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DD5A5B7D5E237E06 ON plan (name)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entitlement CHANGE event_id event_id INT DEFAULT NULL');
        $this->addSql('DROP INDEX UNIQ_3BAE0AA75E237E06 ON event');
        $this->addSql('DROP INDEX UNIQ_DD5A5B7D5E237E06 ON plan');
        $this->addSql('ALTER TABLE plan CHANGE price_override price_override INT DEFAULT NULL');
    }
}
