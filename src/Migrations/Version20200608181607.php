<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200608181607 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE plan_event');
        $this->addSql('ALTER TABLE entitlement CHANGE event_id event_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE plan CHANGE price_override price_override INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE plan_event (plan_id INT NOT NULL, event_id INT NOT NULL, INDEX IDX_68084F5B71F7E88B (event_id), INDEX IDX_68084F5BE899029B (plan_id), PRIMARY KEY(plan_id, event_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE plan_event ADD CONSTRAINT FK_68084F5B71F7E88B FOREIGN KEY (event_id) REFERENCES event (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE plan_event ADD CONSTRAINT FK_68084F5BE899029B FOREIGN KEY (plan_id) REFERENCES plan (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE entitlement CHANGE event_id event_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE plan CHANGE price_override price_override INT DEFAULT NULL');
    }
}
