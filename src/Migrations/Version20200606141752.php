<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200606141752 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE entitlement (id INT AUTO_INCREMENT NOT NULL, expiration_date DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE entitlement_event (entitlement_id INT NOT NULL, event_id INT NOT NULL, INDEX IDX_60A5B95615FCF4DF (entitlement_id), INDEX IDX_60A5B95671F7E88B (event_id), PRIMARY KEY(entitlement_id, event_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE entitlement_event ADD CONSTRAINT FK_60A5B95615FCF4DF FOREIGN KEY (entitlement_id) REFERENCES entitlement (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE entitlement_event ADD CONSTRAINT FK_60A5B95671F7E88B FOREIGN KEY (event_id) REFERENCES event (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entitlement_event DROP FOREIGN KEY FK_60A5B95615FCF4DF');
        $this->addSql('DROP TABLE entitlement');
        $this->addSql('DROP TABLE entitlement_event');
    }
}
