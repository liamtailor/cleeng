<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200609190051 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entitlement CHANGE event_id event_id INT DEFAULT NULL, CHANGE entitlement_set_id entitlement_set_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE plan ADD access_time_override INT DEFAULT NULL, CHANGE price_override price_override INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entitlement CHANGE event_id event_id INT DEFAULT NULL, CHANGE entitlement_set_id entitlement_set_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE plan DROP access_time_override, CHANGE price_override price_override INT DEFAULT NULL');
    }
}
