<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200608162031 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE plan_event_category (plan_id INT NOT NULL, event_category_id INT NOT NULL, INDEX IDX_FF7E901BE899029B (plan_id), INDEX IDX_FF7E901BB9CF4E62 (event_category_id), PRIMARY KEY(plan_id, event_category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE plan_event_category ADD CONSTRAINT FK_FF7E901BE899029B FOREIGN KEY (plan_id) REFERENCES plan (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE plan_event_category ADD CONSTRAINT FK_FF7E901BB9CF4E62 FOREIGN KEY (event_category_id) REFERENCES event_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE entitlement CHANGE event_id event_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE plan CHANGE price_override price_override INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE plan_event_category');
        $this->addSql('ALTER TABLE entitlement CHANGE event_id event_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE plan CHANGE price_override price_override INT DEFAULT NULL');
    }
}
