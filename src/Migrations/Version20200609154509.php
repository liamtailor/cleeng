<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200609154509 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE entitlement_set (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE entitlement ADD entitlement_set_id INT DEFAULT NULL, CHANGE event_id event_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE entitlement ADD CONSTRAINT FK_FA448021DA1AC421 FOREIGN KEY (entitlement_set_id) REFERENCES entitlement_set (id)');
        $this->addSql('CREATE INDEX IDX_FA448021DA1AC421 ON entitlement (entitlement_set_id)');
        $this->addSql('ALTER TABLE plan CHANGE price_override price_override INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entitlement DROP FOREIGN KEY FK_FA448021DA1AC421');
        $this->addSql('DROP TABLE entitlement_set');
        $this->addSql('DROP INDEX IDX_FA448021DA1AC421 ON entitlement');
        $this->addSql('ALTER TABLE entitlement DROP entitlement_set_id, CHANGE event_id event_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE plan CHANGE price_override price_override INT DEFAULT NULL');
    }
}
