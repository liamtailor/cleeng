<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200606143305 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE entitlement_event');
        $this->addSql('ALTER TABLE entitlement ADD event_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE entitlement ADD CONSTRAINT FK_FA44802171F7E88B FOREIGN KEY (event_id) REFERENCES event (id)');
        $this->addSql('CREATE INDEX IDX_FA44802171F7E88B ON entitlement (event_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE entitlement_event (entitlement_id INT NOT NULL, event_id INT NOT NULL, INDEX IDX_60A5B95671F7E88B (event_id), INDEX IDX_60A5B95615FCF4DF (entitlement_id), PRIMARY KEY(entitlement_id, event_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE entitlement_event ADD CONSTRAINT FK_60A5B95615FCF4DF FOREIGN KEY (entitlement_id) REFERENCES entitlement (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE entitlement_event ADD CONSTRAINT FK_60A5B95671F7E88B FOREIGN KEY (event_id) REFERENCES event (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE entitlement DROP FOREIGN KEY FK_FA44802171F7E88B');
        $this->addSql('DROP INDEX IDX_FA44802171F7E88B ON entitlement');
        $this->addSql('ALTER TABLE entitlement DROP event_id');
    }
}
