<?php


namespace App\Api\SubscriberService;

use App\Entity\EntitlementSet;
use App\Entity\ISubscribable;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class DoctrineSubscriberService implements ISubscriberService
{
    /**
     * Typed property inside a service results in an error, so use only annotations
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $entityManager->getRepository(User::class);
    }

    /**
     * @param int $userId
     * @param ISubscribable $subscribeToEntity
     * @return EntitlementSet
     * @throws EntityNotFoundException
     * @throws \LogicException
     * @throws BadRequestException
     */
    public function subscribe(int $userId, ISubscribable $subscribeToEntity)
    {
        /** @var User $user */
        $user = $this->userRepository->find($userId);

        if ($user) {
            $newEntitlements = $subscribeToEntity->subscribe($user);

            $this->entityManager->persist($user);
            $this->entityManager->flush();

            return new EntitlementSet($newEntitlements); // TODO: Needs a factory for the EntitlementSet
        } else {
            throw EntityNotFoundException::fromClassNameAndIdentifier(User::class, [$userId]);
        }
    }

    /**
     * @param int $userId
     * @param ISubscribable $unsubscribeFromEntity
     * @throws EntityNotFoundException
     */
    public function unsubscribe(int $userId, ISubscribable $unsubscribeFromEntity)
    {
        /** @var User $user */
        $user = $this->userRepository->find($userId);

        if ($user) {
            $unsubscribeFromEntity->unSubscribe($user);

            $this->entityManager->persist($user);
            $this->entityManager->flush();
        } else {
            throw EntityNotFoundException::fromClassNameAndIdentifier(User::class, [$userId]);
        }
    }
}