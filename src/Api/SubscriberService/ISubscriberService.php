<?php

namespace App\Api\SubscriberService;

use App\Entity\EntitlementSet;
use App\Entity\ISubscribable;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

interface ISubscriberService
{
    /**
     * @param int $userId
     * @param ISubscribable $subscribeToEntity
     * @return EntitlementSet
     * @throws EntityNotFoundException
     * @throws \LogicException
     * @throws BadRequestException
     */
    public function subscribe(int $userId, ISubscribable $subscribeToEntity);

    /**
     * @param int $userId
     * @param ISubscribable $unsubscribeFromEntity
     * @throws EntityNotFoundException
     */
    public function unsubscribe(int $userId, ISubscribable $unsubscribeFromEntity);
}