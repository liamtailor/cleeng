<?php


namespace App\Api;


use App\Entity\ApiEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Exception\LogicException;
use Symfony\Component\Form\Exception\RuntimeException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class EntityFactoryService
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager)
    {
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormInterface $form
     * @param string $entityClass
     * @return ApiEntity
     * @throws BadRequestException
     * @throws \ReflectionException
     * @throws RuntimeException
     * @throws LogicException
     */
    public function createEntity(FormInterface $form, string $entityClass): ApiEntity
    {
        if ($form->isValid()) {
            $newEntity = $form->getData();
            if ($newEntity instanceof ApiEntity) {
                if ($newEntity instanceof $entityClass) {
                    $this->entityManager->persist($newEntity);
                    $this->entityManager->flush();

                    return $newEntity;
                } else {
                    throw new \ReflectionException('Created object is not of valid class: ' . get_class($newEntity), ' instead of ' . $entityClass);
                }
            } else {
                throw new \ReflectionException('Class must extend ApiEntityClass: ' . $entityClass);
            }
        } else {
            throw new BadRequestException('Invalid entity creation arguments given.');
        }
    }
}