<?php


namespace App\Api;


class Api
{
    /**
     * @param string $url
     * @param string $method
     * @param array $parameters
     * @return mixed
     * @throws \Exception
     */
    public function makeRequest(string $url, array $parameters = [], string $method = 'GET')
    {
        $url = 'http://127.0.0.1:8010/rest/' . ltrim($url, '/');
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($curl);
        if (is_null($response)){
            throw new \Exception('Invalid API request');
        }

        $decodedResponse = json_decode($response);

        if ($decodedResponse === false) {
            // TODO: throw new ApiInvalidResponseException
            throw new \Exception('Invalid JSON in API Response');
        } else {
            return $decodedResponse;
        }
    }
}