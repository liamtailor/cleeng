<?php

namespace App\Api\ApiController;

use App\Api\EntityFactoryService;
use App\Api\SubscriberService\ISubscriberService;
use App\Core\DateTime\DateTimeHelper;
use App\Entity\Plan;
use App\Form\PlanType;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PlanController extends ApiAbstractController
{
    /**
     * @var EntityFactoryService
     */
    private $entityFactoryService;
    /**
     * @var DateTimeHelper
     */
    private $dateTimeHelper;

    public function __construct(EntityFactoryService $entityFactoryService, DateTimeHelper $dateTimeHelper)
    {
        $this->entityFactoryService = $entityFactoryService;
        $this->dateTimeHelper = $dateTimeHelper;
    }

    /**
     * @Route("/rest/plan/{id}", name="plan", methods={"GET"}, requirements={"id"="\d+"})
     * @param int|null $id
     * @return JsonResponse
     */
    public function index(int $id = null): JsonResponse
    {
        return $this->httpGetJsonResponse(Plan::class, $id);
    }

    /**
     * @Route("/rest/plan", name="new-plan", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        try {
            $postData = json_decode($request->getContent(), true);
            $plan = new Plan();
            $form = $this->createForm(PlanType::class, $plan);
            $form->submit($postData);
            if ($form->isSubmitted()) {
                /** @var Plan $newEntity */
                $newEntity = $this->entityFactoryService->createEntity($form, Plan::class);

                $thisRoute = $request->getUri();
                return $this->json($newEntity, 201, ['location' => $thisRoute . $newEntity->getId()]);
            } else {
                return $this->json(['error' => $form->getErrors()], 400);
            }
        } catch (BadRequestException $e) {
            return $this->json(['error: ' . $e->getMessage()], 400);
        } catch (\Exception $e) {
            return $this->json(['error' => 'Server error.'], 500);
        }
    }

    /**
     * @Route("/rest/plan/{planId}/buy/{userId}", name="buy-plan", methods={"POST"}, requirements={"planId"="\d+", "userId"="\d+"})
     * @param int $userId
     * @param int $planId
     * @param ISubscriberService $subscriberService
     * @return JsonResponse
     */
    public function buy(int $userId, int $planId, ISubscriberService $subscriberService)
    {
        try {
            if ($planId && $userId) {
                $entityManager = $this->getDoctrine()->getManager();
                /** @var Plan $plan */
                $plan = $entityManager->getRepository(Plan::class)->find($planId);
                if (is_null($plan)) {
                    throw EntityNotFoundException::fromClassNameAndIdentifier(Plan::class, [$planId]);
                }
                $entitlementSet = $subscriberService->subscribe($userId, $plan);
                foreach ($entitlementSet->getEntitlements() as $entitlement) {
                    if ($plan->getAccessTimeOverride()) {
                        $expirationDateOverride = $this->dateTimeHelper->addDaysToDate(new \DateTime(), $plan->getAccessTimeOverride());
                        $entitlement->setExpirationDate($expirationDateOverride);
                    }
                }

                $entityManager->persist($entitlementSet);
                $entityManager->flush();

                return $this->json($entitlementSet, 201, ['location' => '/rest/entitlementSet/' . $entitlementSet->getId()]);
            }
            throw new BadRequestException('Plan and user ID are required.');
        } catch (EntityNotFoundException $e) {
            return $this->json(['error: Invalid user or plan id.'], 404);
        } catch (BadRequestException $e) {
            return $this->json(['error: ' . $e->getMessage()], 400);
        } catch (\Exception $e) {
            return $this->json(['error' => 'Server error.'], 500);
        }
    }

    /**
     * @Route("/rest/plan/{planId}/unsubscribe/{userId}", name="unsubscribe-from-plan", methods={"PUT"}, requirements={"planId"="\d+", "userId"="\d+"})
     * @param int $userId
     * @param int $planId
     * @param ISubscriberService $subscriberService
     * @return JsonResponse
     */
    public function unsubscribe(int $userId, int $planId, ISubscriberService $subscriberService)
    {
        try {
            if ($planId && $userId) {
                $entityManager = $this->getDoctrine()->getManager();
                /** @var Plan $plan */
                $plan = $entityManager->getRepository(Plan::class)->find($planId);
                if (is_null($plan)) {
                    throw EntityNotFoundException::fromClassNameAndIdentifier(Plan::class, [$planId]);
                }
                $subscriberService->unsubscribe($userId, $plan);

                return $this->json('', 204);
            }
            return $this->json(['error: Invalid request.'], 400);
        } catch (EntityNotFoundException $e) {
            return $this->json('', 404);
        } catch (\LogicException $e) {
            return $this->json(['error' => 'Server error.'], 500);
        }
    }
}
