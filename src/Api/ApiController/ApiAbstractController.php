<?php


namespace App\Api\ApiController;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ApiAbstractController extends AbstractController
{
    /**
     * @param string $entityClass
     * @param int|null $id
     * @return JsonResponse
     */
    protected function httpGetJsonResponse(string $entityClass, int $id = null): JsonResponse
    {
        try {
            $repository = $this->getDoctrine()->getRepository($entityClass);
            if (is_null($id)) {
                $entity = $repository->findAll();
            } else {
                $entity = $this->getEntity($entityClass, $id);
            }

            return $this->json($entity);
        } catch (\LogicException $e) {
            // TODO: Log exception
            return $this->json(['error' => 'Server error.'], 500);
        } catch (NotFoundHttpException $e) {
            return $this->json(['error' => $e->getMessage()], 404);
        }
    }

    /**
     * @param string $entityClass
     * @param string $id
     * @return object[]
     * @throws NotFoundHttpException
     * @throws \LogicException
     */
    protected function getEntity(string $entityClass, string $id)
    {
        $repository = $this->getDoctrine()->getRepository($entityClass);
        $entity = $repository->find($id);
        if ($entity === false) {
            throw new NotFoundHttpException('Entity not found.');
        } else {
            return $entity;
        }
    }
}