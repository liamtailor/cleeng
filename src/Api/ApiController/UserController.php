<?php

namespace App\Api\ApiController;

use App\Api\EntityFactoryService;
use App\Entity\Entitlement;
use App\Entity\User;
use App\Form\UserType;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends ApiAbstractController
{
    /**
     * @var EntityFactoryService
     */
    private $entityFactoryService;

    public function __construct(EntityFactoryService $entityFactoryService)
    {
        $this->entityFactoryService = $entityFactoryService;
    }

    /**
     * @Route("/rest/user/{id}", name="user", methods={"GET"}, requirements={"id"="\d+"})
     * @param int|null $id
     * @return JsonResponse
     */
    public function index(int $id = null): JsonResponse
    {
        return $this->httpGetJsonResponse(User::class, $id);
    }

    /**
     * @Route("/rest/user", name="new-user", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        try {
            $postData = json_decode($request->getContent(), true);
            $plan = new User();
            $form = $this->createForm(UserType::class, $plan);
            $form->submit($postData);
            if ($form->isSubmitted()) {
                /** @var User $newEntity */
                $newEntity = $this->entityFactoryService->createEntity($form, User::class);

                $thisRoute = $request->getUri();
                return $this->json($newEntity, 201, ['location' => $thisRoute . $newEntity->getId()]);
            } else {
                return $this->json(['error' => $form->getErrors()], 400);
            }
        } catch (BadRequestException $e) {
            return $this->json(['error' => $e->getMessage()], 400);
        } catch (\Exception $e) {
            return $this->json(['error' => 'Server error.'], 500);
        }
    }

    /**
     * @Route("/rest/user/name/{name}", name="user-by-name", methods={"GET"})
     * @param string $name
     * @return JsonResponse
     */
    public function getUserByName(string $name = ""): JsonResponse
    {
        try {
            if ($name) {
                $repository = $this->getDoctrine()->getRepository(User::class);
                $user = $repository->findOneBy(['name' => $name]);

                if ($user) {
                    return $this->json($user, 200);
                } else {
                    throw EntityNotFoundException::fromClassNameAndIdentifier(User::class, ['name' => $name]);
                }
            } else {
                throw new BadRequestException('No user name given.');
            }
        } catch (\LogicException $e) {
            return $this->json(['error' => 'Server error.'], 500);
        } catch (EntityNotFoundException $e) {
            return $this->json(['error' => $e->getMessage()], 404);
        } catch (BadRequestException $e) {
            return $this->json(['error' => $e->getMessage()], 400);
        }

    }

    /**
     * @Route("/rest/user/{userId}/entitlements/{entitlementId}", name="user-entitlements", methods={"GET"}, requirements={"userId"="\d+", "entitlementId"="\d+"})
     * @param int|null $userId
     * @param int|null $entitlementId
     * @return JsonResponse
     */
    public function userEntitlements(int $userId = null, int $entitlementId = null): JsonResponse
    {
        try {
            /** @var User $user */
            $user = $this->getEntity(User::class, $userId);
            $entitlements = $user->getEntitlements();
            if (is_null($entitlementId)) {
                return $this->json($entitlements->toArray(), 200);
            } else {
                return $this->httpGetJsonResponse(Entitlement::class, $entitlementId);
            }
        } catch (\LogicException $e) {
            // TODO: Log exception
            return $this->json(['error' => 'Server error.'], 500);
        } catch (NotFoundHttpException $e) {
            return $this->json(['error' => $e->getMessage()], 404);
        }
    }
}
