<?php

namespace App\Api\ApiController;

use App\Entity\EntitlementSet;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class EntitlementSetController extends ApiAbstractController
{
    /**
     * @Route("/rest/entitlementSet/{id}", name="entitlement-set", methods={"GET"}, requirements={"id"="\d+"})
     * @param int|null $id
     * @return JsonResponse
     */
    public function index(int $id = null): JsonResponse
    {
        return $this->httpGetJsonResponse(EntitlementSet::class, $id);
    }

}
