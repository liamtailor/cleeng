<?php

namespace App\Api\ApiController;

use App\Api\EntityFactoryService;
use App\Api\SubscriberService\ISubscriberService;
use App\Core\DateTime\DateTimeHelper;
use App\Entity\Entitlement;
use App\Entity\Event;
use App\Entity\User;
use App\Form\EventType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class EventController extends ApiAbstractController
{
    /**
     * @var DateTimeHelper
     */
    private $dateTimeHelper;
    /**
     * @var EntityFactoryService
     */
    private $entityFactoryService;

    public function __construct(EntityFactoryService $entityFactoryService, DateTimeHelper $dateTimeHelper)
    {
        $this->dateTimeHelper = $dateTimeHelper;
        $this->entityFactoryService = $entityFactoryService;
    }

    /**
     * @Route("/rest/event/{id}", name="event", methods={"GET"}, requirements={"id"="\d+"})
     * @param int|null $id
     * @return JsonResponse
     */
    public function index(int $id = null): JsonResponse
    {
        return $this->httpGetJsonResponse(Event::class, $id);
    }

    /**
     * @Route("/rest/event", name="new-event", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        // TODO: Every entity is probably created this way, so this could be abstracted.
        try {
            $postData = json_decode($request->getContent(), true);
            $event = new Event();
            $form = $this->createForm(EventType::class, $event);
            $form->submit($postData);
            if ($form->isSubmitted()) {
                /** @var Event $newEntity */
                $newEntity = $this->entityFactoryService->createEntity($form, Event::class);

                $thisRoute = $request->getUri();
                return $this->json($newEntity, 201, ['location' => $thisRoute . $newEntity->getId()]);
            } else {
                throw new BadRequestException($form->getErrors());
            }
        } catch (BadRequestException $e) {
            return $this->json($e->getMessage(), 400);
        } catch (\Exception $e) {
            return $this->json(['error' => 'Server error.'], 500);
        }
    }

    /**
     * @Route("/rest/event/{eventId}/buy/{userId}", name="buy-event", methods={"POST"},requirements={"eventId"="\d+", "userId"="\d+"})
     * @param int $userId
     * @param int $eventId
     * @param ISubscriberService $subscriberService
     * @return JsonResponse
     */
    public function buy(int $userId, int $eventId, ISubscriberService $subscriberService)
    {
        try {
            if ($eventId && $userId) {
                $manager = $this->getDoctrine()->getManager();
                /** @var Event $event */
                $event = $manager->getRepository(Event::class)->find($eventId);
                if (is_null($event)) {
                    throw EntityNotFoundException::fromClassNameAndIdentifier(Event::class, [$eventId]);
                }
                $entitlementSet = $subscriberService->subscribe($userId, $event);
                $newEntitlements = $entitlementSet->getEntitlements();
                if (count($newEntitlements) !== 1) {
                    foreach ($newEntitlements as $entitlement) {
                        $manager->remove($entitlement);
                        $manager->remove($entitlementSet);
                        $manager->flush();
                    }
                    throw new \LogicException('Subscription to event should always create a single Entitlement.');
                } else {
                    $newEntitlement = $newEntitlements[0];
                    return $this->json($newEntitlement, 201, ['location' => '/rest/entitlement/' . $newEntitlement->getId()]);
                }
            }
            throw new BadRequestException('Event and user id required.');
        } catch (EntityNotFoundException $e) {
            return $this->json(['error' => $e->getMessage()], 404);
        } catch (BadRequestException $e) {
            return $this->json(['error' => $e->getMessage()], 400);
        } catch (\Exception $e) {
            // TODO: Log Exception
            return $this->json(['error' => 'Server error.'], 500);
        }
    }

    /**
     * @Route("/rest/event/{eventId}/unsubscribe/{userId}", name="unsubscribe-from-event", methods={"PUT"}, requirements={"eventId"="\d+", "userId"="\d+"})
     * @param int $userId
     * @param int $eventId
     * @param ISubscriberService $subscriberService
     * @return JsonResponse
     */
    public function unsubscribe(int $userId, int $eventId, ISubscriberService $subscriberService)
    {
        try {
            if ($eventId && $userId) {
                $manager = $this->getDoctrine()->getManager();
                /** @var Event $event */
                $event = $manager->getRepository(Event::class)->find($eventId);
                if (is_null($event)) {
                    throw EntityNotFoundException::fromClassNameAndIdentifier(Event::class, [$eventId]);
                }
                $subscriberService->unsubscribe($userId, $event);

                return $this->json('', 204);
            }
            return $this->json(['error' => 'User and event id required.'], 400);
        } catch (EntityNotFoundException $e) {
            return $this->json(['error' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            return $this->json(['error' => 'Server error.'], 500); // TODO: Maybe JSONResponse factory based on Exception class + custom ApiException classes?
        }
    }

    /**
     * @Route("/rest/event/{eventId}/watch/{userId}", name="user-watch-event", methods={"GET"}, requirements={"eventId"="\d+", "userId"="\d+"})
     * @param int $userId
     * @param int $eventId
     * @return JsonResponse
     */
    public function watch(int $userId, int $eventId)
    {
        try {
            if ($userId && $eventId) {
                $userRepository = $this->getUserRepository();
                /** @var User $user */
                $user = $userRepository->find($userId);
                if ($user) {
                    $entitlements = $user->getEntitlements();
                    /** @var Entitlement $entitlement */
                    foreach ($entitlements->toArray() as $entitlement) {
                        $entitlementExpirationDate = $entitlement->getExpirationDate();
                        // TODO: Expired entitlements cleanup.
                        if ($entitlement->getEvent()->getId() === $eventId && $entitlement->isNotExpired()) {
                            // TODO: How to prevent user from just going to video URL? .htaccess / FileAccessController / maybe another method?
                            $videos = [
                                '/videos/cat_crash.webm',
                                '/videos/falling_cat.webm',
                            ];
                            return $this->json(['video' => $videos[rand(0, 1)], 'expirationDate' => $entitlementExpirationDate], 200);
                        }
                    }
                    return $this->json('Forbidden', 403);
                } else {
                    throw EntityNotFoundException::fromClassNameAndIdentifier(User::class, [$userId]);
                }
            }
            return $this->json(['error' => 'User and event id required.'], 400);
        } catch (EntityNotFoundException $e){
            return $this->json(['error' => $e->getMessage()], 404);
        }
    }

    private function getUserRepository(): UserRepository
    {
        return $this->getDoctrine()->getRepository(User::class);
    }
}
