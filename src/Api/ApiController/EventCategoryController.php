<?php

namespace App\Api\ApiController;

use App\Api\EntityFactoryService;
use App\Entity\Event;
use App\Entity\EventCategory;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class EventCategoryController extends ApiAbstractController
{
    /**
     * @var EntityFactoryService
     */
    private $entityFactoryService;

    public function __construct(EntityFactoryService $entityFactoryService)
    {
        $this->entityFactoryService = $entityFactoryService;
    }

    /**
     * @Route("/rest/eventCategory/{id}", name="event-category", methods={"GET"}, requirements={"id"="\d+"})
     * @param int|null $id
     * @return JsonResponse
     */
    public function index(int $id = null): JsonResponse
    {
        return $this->httpGetJsonResponse(EventCategory::class, $id);
    }

    /**
     * @Route("/rest/eventCategory", name="new-event", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        // TODO: Every entity is probably created this way, so this could be abstracted.
        try {
            $postData = json_decode($request->getContent(), true);
            $eventCategory = new EventCategory();
            $form = $this->createForm(EventCategory::class, $eventCategory);
            $form->submit($postData);
            if ($form->isSubmitted()) {
                /** @var Event $newEntity */
                $newEntity = $this->entityFactoryService->createEntity($form, EventCategory::class);

                $thisRoute = $request->getUri();
                return $this->json($newEntity, 201, ['location' => $thisRoute . $newEntity->getId()]);
            } else {
                throw new BadRequestException($form->getErrors());
            }
        } catch (BadRequestException $e) {
            return $this->json($e->getMessage(), 400);
        } catch (\Exception $e) {
            return $this->json(['error' => 'Server error.'], 500);
        }
    }
}
