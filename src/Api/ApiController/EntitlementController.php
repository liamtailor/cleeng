<?php

namespace App\Api\ApiController;

use App\Entity\Entitlement;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class EntitlementController extends ApiAbstractController
{
    /**
     * @Route("/rest/entitlement/{id}", name="entitlement", methods={"GET"}, requirements={"id"="\d+"})
     * @param int|null $id
     * @return JsonResponse
     */
    public function index(int $id = null): JsonResponse
    {
        return $this->httpGetJsonResponse(Entitlement::class, $id);
    }

}
