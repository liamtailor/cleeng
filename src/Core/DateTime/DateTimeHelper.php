<?php


namespace App\Core\DateTime;


class DateTimeHelper
{
    /**
     * @param \DateTime $dateTime
     * @param int $daysToAdd
     * @return \DateTime
     * @throws \Exception
     */
    public function addDaysToDate(\DateTime $dateTime, int $daysToAdd)
    {
        return new \DateTime("+" . $daysToAdd . " day " . $dateTime->format('Y-m-d'));
    }
}