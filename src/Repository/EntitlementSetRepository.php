<?php

namespace App\Repository;

use App\Entity\EntitlementSet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EntitlementSet|null find($id, $lockMode = null, $lockVersion = null)
 * @method EntitlementSet|null findOneBy(array $criteria, array $orderBy = null)
 * @method EntitlementSet[]    findAll()
 * @method EntitlementSet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EntitlementSetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EntitlementSet::class);
    }

    // /**
    //  * @return EntitlementSet[] Returns an array of EntitlementSet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EntitlementSet
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
